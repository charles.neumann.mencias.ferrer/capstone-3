import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../AuthContext';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

const Home = () => {
  const [activeProducts, setActiveProducts] = useState([]);
  const navigate = useNavigate();
  const { isLoggedIn, user, addProduct } = useContext(AuthContext);

  useEffect(() => {
    const fetchActiveProducts = async () => {
      try {
        if (user && user.token) {
          const response = await fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
            method: 'GET',
            headers: {
              Authorization: `Bearer ${user.token}`,
            },
          });
          const data = await response.json();
          setActiveProducts(data);
        }
      } catch (error) {
        console.log('Error fetching active products:', error);
      }
    };

    fetchActiveProducts();
  }, [user]);

  const handleProductClick = (productId) => {
    navigate(`/products/${productId}`);
  };

  const handleAddToCart = (productId) => {
    addProduct(productId);
  };

  return (
    <div className="home">
      <h2>Products</h2>
      <div className="card-container">
        {activeProducts.map((product) => (
          <Card key={product._id} style={{ width: '18rem' }}>
            <Card.Img
              src={`${process.env.PUBLIC_URL}/images/${product.name}/image.jpg`}
              className="card-img-top"
              alt="Image"
              onMouseOver={(e) => (e.currentTarget.src = `${process.env.PUBLIC_URL}/images/${product.name}/animated.gif`)}
              onMouseOut={(e) => (e.currentTarget.src = `${process.env.PUBLIC_URL}/images/${product.name}/image.jpg`)}
              onClick={() => handleProductClick(product._id)}
            />
            <Card.Body>
              <Card.Title>{product.name}</Card.Title>
              <Card.Text>Price: {product.price}</Card.Text>
              {isLoggedIn && !user.isAdmin && (
                <Button onClick={() => handleAddToCart(product._id)}>Add to Cart</Button>
              )}
            </Card.Body>
          </Card>
        ))}
      </div>
    </div>
  );
};

export default Home;
