import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import AuthContext from './AuthContext';
import Navbar from './Navbar';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import AllProducts from './pages/AllProducts';
import Product from './pages/Product';
import CheckoutCart from './pages/CheckoutCart';

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState(() => {
    const storedUser = localStorage.getItem('user');
    return storedUser
      ? JSON.parse(storedUser)
      : { id: null, isAdmin: false, token: null, name: '' };
  });

  const unsetUser = () => {
    localStorage.removeItem('user');
  };

  const loginHandler = (userData) => {
    setIsLoggedIn(true);
    setUser(userData);
  };

  const logoutHandler = () => {
    setIsLoggedIn(false);
    setUser({
      id: null,
      isAdmin: false,
      token: null,
      name: '',
    });
  };

  const [cart, setCart] = useState(() => {
    const storedCart = localStorage.getItem('cart');
    return storedCart ? JSON.parse(storedCart) : { userId: null, products: [] };
  });

  const addProduct = (productId) => {
    setCart((prevData) => {
      const existingProductIndex = prevData.products.findIndex(
        (product) => product.productId === productId
      );
      if (existingProductIndex !== -1) {
        const updatedProducts = [...prevData.products];
        updatedProducts[existingProductIndex].quantity += 1;
        return { ...prevData, products: updatedProducts };
      } else {
        const newProduct = { productId, quantity: 1 };
        const updatedProducts = [...prevData.products, newProduct];
        return {
          ...prevData,
          userId: prevData.userId || user.id,
          products: updatedProducts,
        };
      }
    });
  };

  const subtractProduct = (productId, quantity) => {
    setCart((prevData) => ({
      ...prevData,
      products: prevData.products.map((product) => {
        if (product.productId === productId) {
          return {
            ...product,
            quantity: Math.max(product.quantity - quantity, 0),
          };
        }
        return product;
      }),
    }));
  };

  const clearCart = () => {
    setCart({ userId: null, products: [] });
  };

  useEffect(() => {
    // Invoke any necessary logic here when the page starts
    console.log('Page started');

    // Clean up the effect if necessary
    return () => {
      // Clean up code here
    };
  }, []);

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn,
        login: loginHandler,
        logout: logoutHandler,
        user,
        unsetUser,
        addProduct,
        subtractProduct,
        cart,
        setCart,
        clearCart,
      }}
    >
      <Router>
        <div className="App">
          <Navbar />
          <div className="content">
            <Routes>
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/products/:productId" element={<Product cart={cart} />} />
              {user.isAdmin && <Route path="/products/all" element={<AllProducts cart={cart} />} />}
              {!user.isAdmin && <Route path="/cart" element={<CheckoutCart />} />}
              {isLoggedIn && <Route path="/" element={<Home />} />}
              {!isLoggedIn && <Route path="/" element={<Login />} />}
            </Routes>
          </div>
          {/*isLoggedIn && <button onClick={clearCart}>Clear Cart</button>*/}
        </div>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
